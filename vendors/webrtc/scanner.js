var videobox = document.getElementById('video-box');
var video = document.getElementById("video");
var resultDiv = document.getElementById("result");
var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");
var checkbox = document.getElementById("checkbox");

var width = parseInt(videobox.offsetWidth);
var height = parseInt(videobox.offsetWidth);
canvas.width = width;
canvas.height = height;
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

function gotSources(sourceInfos) {
	for (var i = 0; i !== sourceInfos.length; ++i) {
		var sourceInfo = sourceInfos[i];
		var option = document.createElement('option');

		option.value = sourceInfo.deviceId;

		if (sourceInfo.kind === 'videoinput') {
			option.text = sourceInfo.label || 'camera ' + (videoSelect.length + 1);
			videoSelect.appendChild(option);
		} else {
			console.log('Some other kind of source: ', sourceInfo);
		}
	}
}

if (navigator.getUserMedia){
	function successCallback(stream){
		if (window.webkitURL) {
			video.src = window.webkitURL.createObjectURL(stream);
		} else if (video.mozSrcObject !== undefined) {
			video.mozSrcObject = stream;
		} else {
			video.src = stream;
		}
	}

	function errorCallback(){}
	navigator.getUserMedia({video: true}, successCallback, errorCallback);
	requestAnimationFrame(tick);
}

function tick(){
	requestAnimationFrame(tick);
	if (video.readyState === video.HAVE_ENOUGH_DATA){
		// Load the video onto the canvas
		context.drawImage(video, 0, 0, width, height);

		// Load the image data from the canvas
		var imageData = context.getImageData(0, 0, width, height);

		if (checkbox.checked) { // We want to extract the location of the QR code, so use the seperate functions
			var binarizedImage = jsQR.binarizeImage(imageData.data, imageData.width, imageData.height);
			var location = jsQR.locateQRInBinaryImage(binarizedImage);

			if(!location){
				resultDiv.innerHTML = "<div style='color: red; margin:15px;'>No QR Decoded</div>";
				return;
			}

			var rawQR = jsQR.extractQRFromBinaryImage(binarizedImage, location);
			if (!rawQR) {
				resultDiv.innerHTML = "<div style='color: red; margin:15px;'>No QR Decoded</div>";
				return;
			}

			decoded = jsQR.decodeQR(rawQR)

			if(decoded) {
				resultDiv.innerHTML = "<div style='color: green; margin:15px;'>QR Decoded! <span style='color: #000;'>"+decoded+"</span></div>"
				context.beginPath();

				context.moveTo(location.bottomLeft.x, location.bottomLeft.y);
				context.lineTo(location.topLeft.x, location.topLeft.y);
				context.lineTo(location.topRight.x, location.topRight.y);
				context.lineWidth = 4;
				context.strokeStyle = "green";
				context.stroke();
			} else {
				resultDiv.innerHTML = "<div style='color: red; margin:15px;'>No QR Decoded</div>";
			}
		} else { // We just want to parse the QR code, so we can use the wrapper function
			var decoded = jsQR.decodeQRFromImage(imageData.data, imageData.width, imageData.height);

			if(decoded) {
				resultDiv.innerHTML = "<div style='color: green; margin:15px;'>QR Decoded! <span style='color: #000;'>"+decoded+"</span></div>"
			} else {
				resultDiv.innerHTML = "<div style='color: red; margin:15px;'>No QR Decoded</div>"
			}
		}
	}
}